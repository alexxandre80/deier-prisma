const { createUser, updateUser } = require('./Mutations/mutationUser')
const { createAnimal, updateAnimal, deleteAnimal } = require('./Mutations/mutationAnimal')
const { createTransaction, updateTransaction } = require('./Mutations/mutationTransaction')
const { createCertification, updateCertification } = require('./Mutations/mutationCertification')
const { createCommentaire, updateCommentaire } = require('./Mutations/mutationCommentaire')
const { login, signup } = require('./auth')

const Mutation = {
    createUser,
    updateUser,

    createTransaction,
    updateTransaction,

    createAnimal,
    updateAnimal,
    deleteAnimal,

    createCertification,
    updateCertification,

    createCommentaire,
    updateCommentaire,

    login,
    signup
}

module.exports = {
    Mutation
}