const { forwardTo } = require('prisma-binding')
const { getUser } = require('../../utils')

async function createAnimal (parent, args, ctx, info) {
  const requesterUser = await getUser(ctx)
  args.data.proprio.connect.id = requesterUser.id
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateAnimal (parent, args, ctx, info) {
  //TODO : forwardTo if user === proprio || user.role === ADMIN
  return forwardTo('prisma')(parent, args, ctx, info)
}
async function deleteAnimal (parent, args, ctx, info) {
  //TODO : forwardTo if user === proprio || user.role === ADMIN
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
  createAnimal,
  updateAnimal,
  deleteAnimal
}