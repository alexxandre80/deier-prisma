const { forwardTo } = require('prisma-binding')
const { getUser } = require('../../utils')

async function createCertification (parent, args, ctx, info) {
  const requesterUser = await getUser(ctx)
  args.data.user.connect.id = requesterUser.id
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateCertification (parent, args, ctx, info) {
  //TODO : forwardTo if user === diplomé || user.role === ADMIN
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
  createCertification,
  updateCertification
}