const { forwardTo } = require('prisma-binding')
const { getUser } = require('../../utils')

async function createTransaction (parent, args, ctx, info) {
  const requesterUser = await getUser(ctx)
  args.data.acheteur.connect.id = requesterUser.id
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateTransaction (parent, args, ctx, info) {
  //TODO : forwardTo if user === buyer or seller || user.role === ADMIN
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
  createTransaction,
  updateTransaction
}