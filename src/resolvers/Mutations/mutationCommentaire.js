const { forwardTo } = require('prisma-binding')
const { getUser } = require('../../utils')

async function createCommentaire (parent, args, ctx, info) {
  const requesterUser = await getUser(ctx)
  args.data.author.connect.id = requesterUser.id
  return forwardTo('prisma')(parent, args, ctx, info)
}

async function updateCommentaire (parent, args, ctx, info) {
  //TODO : forwardTo if user === author or sujet || user.role === ADMIN
  return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
  createCommentaire,
  updateCommentaire
}