const { forwardTo } = require('prisma-binding')

async function certification (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function certifications (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    certification,
    certifications
  }