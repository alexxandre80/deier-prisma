const { forwardTo } = require('prisma-binding')

async function transaction (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function transactions (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    transaction,
    transactions
  }