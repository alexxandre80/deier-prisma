const { forwardTo } = require('prisma-binding')

async function commentaire (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function commentaires (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    commentaire,
    commentaires
  }