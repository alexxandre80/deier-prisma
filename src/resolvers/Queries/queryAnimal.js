const { forwardTo } = require('prisma-binding')

async function animal (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

async function animals (parent, args, ctx, info) {
    return forwardTo('prisma')(parent, args, ctx, info)
}

module.exports = {
    animal,
    animals
  }