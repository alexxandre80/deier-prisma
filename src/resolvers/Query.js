const { me } = require('./auth')
const { user, users } = require('./Queries/queryUser')
const { transaction, transactions } = require('./Queries/queryTransaction')
const { animal, animals } = require('./Queries/queryAnimal')
const { certification, certifications } = require('./Queries/queryCertification')
const { commentaire, commentaires } = require('./Queries/queryCommentaire')

const Query = {
    me,
    user,
    users,
    transaction,
    transactions,
    animal,
    animals,
    certification,
    certifications,
    commentaire,
    commentaires
}

module.exports = {
    Query
}